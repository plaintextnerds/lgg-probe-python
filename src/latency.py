"""
VERSION: Demo - 1.1.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2022
"""

import select
import socket
from datetime import datetime
from typing import Dict, KeysView, ItemsView

from dataping import DataPing, MILLISECONDS


class Latency:
    def __init__(self, a_af: int = socket.AF_INET, a_timeout: int = 2000):
        self.m_pings: Dict[str, DataPing] = {}
        self.m_af: int = a_af
        self.m_timeout = a_timeout
        self.m_start_time = int(datetime.now().timestamp() * MILLISECONDS)
        self.m_killed = False
        self.m_stopped = False

    def __getitem__(self, item) -> DataPing:
        return self.m_pings[item]

    def __delitem__(self, item):
        del self.m_pings[item]

    def items(self) -> ItemsView[str, DataPing]:
        return self.m_pings.items()

    def __len__(self) -> int:
        return len(self.m_pings)

    def keys(self) -> KeysView[str]:
        return self.m_pings.keys()

    def __add__(self, a_ping: DataPing):
        self.m_pings[a_ping.m_target_ip] = a_ping

    def add(self, a_ping: DataPing):
        self.__add__(a_ping)

    def run(self):
        if self.m_af == socket.AF_INET:
            port = 9998
        elif self.m_af == socket.AF_INET6:
            port = 9999
        else:
            raise ValueError("invalid AF")
        sock = socket.socket(self.m_af, socket.SOCK_DGRAM)
        if self.isRunnable():
            for target, ping in self.m_pings.items():
                sock.sendto(ping.generatePacket(), (target, port))
        while self.isRunnable():
            readable_sockets, _, _ = select.select(
                [sock], [], [], (self.m_timeout / 1000) / 3
            )
            if not readable_sockets:
                continue
            data, address = sock.recvfrom(1500)
            if address[0] in self.m_pings:
                ping: DataPing = self.m_pings[address[0]]
                if not ping.update(data, len(data)):
                    continue
                if not (ping.isComplete() and not ping.isDead()):
                    sock.sendto(ping.generatePacket(), address)

    def isRunnable(self) -> bool:
        runnable = False
        timestamp = int(datetime.now().timestamp() * MILLISECONDS)
        for _, ping in self.m_pings.items():
            if (
                not (ping.isComplete())
                and not (ping.isDead())
                and (timestamp - self.m_start_time < self.m_timeout)
            ):
                runnable = True
        return runnable

    def isComplete(self) -> bool:
        complete = True
        for _, ping in self.m_pings.items():
            if not (ping.isComplete()):
                complete = False
        return complete

    def stop(self):
        self.m_stopped = True

    def kill(self):
        self.m_killed = True
        killed_targets = []
        for target, ping in self.m_pings.items():
            if not (ping.isComplete()):
                ping.kill()
                killed_targets.append(ping)
        for target in killed_targets:
            del self.m_pings[target]
