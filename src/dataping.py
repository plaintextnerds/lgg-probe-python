"""
VERSION: Demo - 1.1.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2022
"""
import json
from dataclasses import dataclass, field
from enum import Enum
from functools import partial
from random import randint
from datetime import datetime
from statistics import mean, stdev
from typing import List, cast, Union, Callable

from statemachine import StateMachine, State  # type: ignore

from wire_formats import (
    DataPingRequestV3,
    DataPingRequestV2,
    DataPingResponseV2,
    EchoClientServerType,
    EchoServerClientType,
)


MILLISECONDS = 1000  # Convert from seconds to milliseconds


class Valid(Enum):
    Unknown = -1
    Invalid = 0
    Valid = 1


class MeasurementState(StateMachine):
    c0 = State("C0", initial=True)
    s0 = State("S0")
    c1 = State("C1")
    s1 = State("S1")
    c2 = State("C2")
    complete = State("complete")

    recv_s0 = c0.to(s0)
    send_c1 = s0.to(c1)
    recv_s1 = c1.to(s1)
    send_c2 = s1.to(c2)
    recv_s2 = c2.to(complete)


@dataclass
class Stats:
    m_rtt: int = field()
    m_stddev: int = field()


class DataPing:
    def __init__(self, a_target_ip: str, a_source_ip: str, a_ident: str, a_token: str):
        self.m_state: MeasurementState = MeasurementState()
        self.m_version = "v0.0.0-local"
        self.m_token = a_token
        self.m_ident = a_ident
        self.m_target_ip = a_target_ip
        self.m_source_ip = a_source_ip
        self.m_timestamps: List[int] = [0, 0, 0, 0, 0, 0, 0]
        self.m_signatures: List[int] = [0, 0, 0]
        self.m_seq = randint(0, 255)  # nosec
        self.m_dpversion = 3
        self.m_errors = 0

    def tryV2(self):
        self.m_dpversion = 2

    def update(self, data: bytes, length) -> bool:
        success = True
        if length != 112:
            self.m_errors += 1
            success = False
        else:
            packet: DataPingResponseV2 = DataPingResponseV2.from_buffer_copy(data)
            if packet.type == EchoServerClientType.Null:
                self.m_errors += 1
                success = False
            elif packet.type == EchoServerClientType.Initial and self.m_state.is_c0:
                self.m_timestamps[1] = packet.timestamp
                self.m_state.recv_s0()
            elif packet.type == EchoServerClientType.Next and self.m_state.is_c1:
                self.m_timestamps[3] = packet.timestamp
                self.m_state.recv_s1()
            elif packet.type == EchoServerClientType.Final and self.m_state.is_c2:
                self.m_timestamps[5] = packet.timestamp
                self.m_timestamps[6] = int(datetime.now().timestamp() * MILLISECONDS)
                self.m_state.recv_s2()
        if self.m_errors == 3 and self.m_dpversion == 3:
            self.m_errors = 0
            self.tryV2()
        return success

    def generatePacket(self) -> bytes:
        packet: bytes
        if self.m_dpversion == 3:
            packet = self.generatePacketV3()
        else:
            packet = self.generatePacketV2()
        return packet

    def generatePacketV3(self) -> bytes:
        timestamp = int(datetime.now().timestamp() * MILLISECONDS)
        packet_type = EchoClientServerType.Initial
        if self.m_state.is_c0:
            self.m_timestamps[0] = timestamp
        elif self.m_state.current_state in (MeasurementState.s0, MeasurementState.c1):
            if self.m_state.is_s0:
                self.m_state.send_c1()
            packet_type = EchoClientServerType.Second
            self.m_timestamps[2] = timestamp
        elif self.m_state.current_state in (MeasurementState.s1, MeasurementState.c2):
            if self.m_state.is_s1:
                self.m_state.send_c2()
            packet_type = EchoClientServerType.Final
            self.m_timestamps[4] = timestamp
        return cast(
            bytes,
            DataPingRequestV3(
                version=3,
                type=packet_type,
                seq=self.m_seq,
                token=self.m_token.encode(),
                timestamp=timestamp,
            ),
        )

    def generatePacketV2(self) -> bytes:
        timestamp = int(datetime.now().timestamp() * MILLISECONDS)
        packet_type = EchoClientServerType.Initial
        if self.m_state.is_c0:
            self.m_timestamps[0] = timestamp
        elif self.m_state.current_state in (MeasurementState.s0, MeasurementState.c1):
            if self.m_state.is_s0:
                self.m_state.send_c1()
            packet_type = EchoClientServerType.Second
            self.m_timestamps[2] = timestamp
        elif self.m_state.current_state in (MeasurementState.s1, MeasurementState.c2):
            if self.m_state.is_s1:
                self.m_state.send_c2()
            packet_type = EchoClientServerType.Final
            self.m_timestamps[4] = timestamp
        return cast(
            bytes,
            DataPingRequestV2(
                version=3,
                type=packet_type,
                seq=self.m_seq,
                token=self.m_ident.encode(),
                timestamp=timestamp,
            ),
        )

    def isComplete(self) -> bool:
        return self.m_state.is_complete

    def isDead(self) -> bool:
        return self.m_errors >= 3 and self.m_version == 2

    def getStats(self) -> Stats:
        raw_rtts = [
            self.m_timestamps[2] - self.m_timestamps[0],
            self.m_timestamps[3] - self.m_timestamps[1],
            self.m_timestamps[4] - self.m_timestamps[2],
            self.m_timestamps[5] - self.m_timestamps[3],
            self.m_timestamps[6] - self.m_timestamps[4],
        ]
        p_max = cast(Callable, partial(max, 0))
        p_min = cast(Callable, partial(min, 500))
        corrected_rtts: List[Union[int, float]] = list(map(p_max, map(p_min, raw_rtts)))
        stats = Stats(
            int(cast(float, mean(corrected_rtts))),
            int(cast(float, stdev(corrected_rtts))),
        )
        return stats

    def asJson(self) -> str:
        stats = self.getStats()
        sample = {
            "type": "udp-data",
            "version": self.m_version,
            # "ip": self.m_source_ip,
            # "ident": self.m_token,
            "rtt": stats.m_rtt,
            "stddev": stats.m_stddev,
            # "raw": {
            #     "timestamp_c0": self.m_timestamps[0],
            #     "timestamp_c1": self.m_timestamps[2],
            #     "timestamp_c2": self.m_timestamps[4],
            #     "timestamp_c3": self.m_timestamps[6],
            #     "timestamp_s0": self.m_timestamps[1],
            #     "timestamp_s1": self.m_timestamps[3],
            #     "timestamp_s2": self.m_timestamps[5],
            #     "signatures": self.m_signatures,
            #     "errors": self.m_errors,
            # },
        }
        return json.dumps(sample)

    def kill(self):
        self.m_errors = 3
        self.m_version = 2
