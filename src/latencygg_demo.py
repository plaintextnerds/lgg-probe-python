"""
VERSION: Demo - 1.1.0
LICENSE: WTFPL
COPYRIGHT: Latency.GG - 2022
"""
import json
import random
import socket
from typing import cast

import click

from backend_api import get_metrics, Beacon
from dataping import DataPing
from latency import Latency


@click.command()
@click.option(
    "--backend-api",
    envvar="LATENCYGG_BACKEND_API",
    prompt="Backend API URL",
    help="The URL of the Backend (e.g. https://demo.latency.gg)",
)
def main(
    backend_api: str,
):
    source, clean_metrics, stale_metrics = get_metrics(backend_api)
    source_ip: str = source["addr"]
    version: int = source["version"]
    ident: str = source["ident"]
    latency = Latency(socket.AF_INET if version == 4 else socket.AF_INET6, 2000)
    target_lookup = {}
    target_ip: str
    for provider_name, locations in stale_metrics.items():
        for location_name, location in locations.items():
            selected = cast(Beacon, random.choice(cast(list, location.get("beacons", [None]))))  # nosec
            if selected is not None:
                target_ip = cast(str, selected[f"ipv{version}"])
                dp = DataPing(target_ip, source_ip, ident, cast(str, selected["token"]))
                latency.add(dp)
                target_lookup[target_ip] = [provider_name, location_name]
    latency.run()
    final_data = clean_metrics
    for target, ping in latency.items():
        if ping.isComplete():
            provider_name, location_name = target_lookup[target]
            if location_name not in final_data[provider_name]:
                final_data[provider_name][location_name] = json.loads(ping.asJson())
    for provider_name, final_locations in final_data.items():
        print(provider_name)
        for location_name, result in final_locations.items():
            print(f"\t{location_name}: {result['rtt']} (~{result['stddev']})")
